# `computing.hardware.autonomy`
Dedicated towards enabling the mass distribution of self-production-capable tools to engineer and manufacture nanotechology. 

This begins with a focus on computational hardware, but the same tools should be capable of extremely sophisticated manufacture of most nanostructures - including microfluidics and many more - with the implied scale of radical societal change that this implies. 

Repositories:
* [`gitlab-profile`](https://gitlab.com/computing.hardware.autonomy/gitlab-profile): this README.md file, basically
* [`modelling`](https://gitlab.com/computing.hardware.autonomy/modelling) - Libraries and other code that enables effective simulation of devices. This is a "sometime in near future" thing.
* [`fabrication`](https://gitlab.com/computing.hardware.autonomy/fabrication) - Documentation, diagrams, models, etc. on various fabrication methods, technologies, designs, research, etc. - this is the highest priority repository. It also keeps some copies and development-copies of [RepRap Wiki](https://reprap.org/wiki) pages.
  This repository is the most important as of time of writing, because getting some ideas and designs out there is critical to avoid patent encumberment. 

## Licensing:
* `AGPLv3+` - for software and anything software-like that might be used as a hardware schematic. All software and documentation is licensed under this unless stated otherwise. Software that is *only* software (i.e. isn't designed for use when encoding hardware schematics) can use just this license rather than dual-licensing with `CERN-OHLv2+-S`
* `CERN OHLv2+-S` - for hardware and anything that may be used in a hardware schematic (e.g. software). All software, documentation, and hardware schematics are licensed under this as well, unless stated otherwise.
* `CC-BY-SA-4.0` - documentation and final output diagrams are licensed under this as well for compatibility with RepRap wiki
* `GFDL1.2` - documentation and final output diagrams are licensed under this as well for compatibility with RepRap wiki.

In summary, if you are:
* *Contributing* to software in this project, you should license it under `AGPLv3+` and `OHLv2+-S`, unless it's software that is not designed to also allow hardware models depending on it, in which case you should only license under `AGPLv3+` (if in doubt, follow the project you're contributing to, or dual-license). 
* *Writing* your own software depending upon software by this project, you can license under `AGPLv3+`-only (though if you're writing software that extends the paradigm of encoding hardware information as code, consider carrying on the dual-license mechanism of `AGPLv3+` and `OHLv2+-S`)
* *Contributing* to hardware schematics in this project, you should license it under `AGPLv3+` and `OHLv2+-S`, unless it's *only* a hardware schematic and not really a library in which case you can license it under exclusively `OHLv2+-S` (if in doubt, follow the project you're contributing to, or dual-license). 
* *Writing* your own hardware schematics depending on mixed hardware/software libraries, you can license under `OHLv2+-S`-only (though if you're writing schematics that extend the paradigm of encoding hardware information as code, consider carrying on the dual-license mechanism of `AGPLv3+` and `OHLv2+-S`)
* Contributing to wiki pages or documentation or diagrams in this project, you should license it under `AGPLv3+`, `OHLv2+-S`, `CC-BY-SA-4.0`, and `GFDL1.2`. It seems like a lot, but the last two are specifically for compatibility with the RepRap wiki. Some RepRap wiki pages are licensed only under `GFDL1.2`, and in that case the `GFDL1.2` part allows us to make the modification to the RepRap page, and the rest allow free interaction with the other project components.

When contributing, unless specified otherwise, you agree that you're contributing code under these licenses (or a license compatible with them).
